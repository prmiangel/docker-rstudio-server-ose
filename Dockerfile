FROM ubuntu:21.04

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt install -y dirmngr gnupg apt-transport-https ca-certificates software-properties-common
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
RUN add-apt-repository -y "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"
RUN apt-get install -y --no-install-recommends r-base r-base-dev

RUN apt-get install -y wget 
RUN apt-get install -y gdebi-core
RUN wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-1.4.1717-amd64.deb
RUN gdebi -n rstudio-server-1.4.1717-amd64.deb

EXPOSE 8787

#ENTRYPOINT ["rstudio-server", "start"]
#CMD ["rstudio-server", "start"]

#ENTRYPOINT [ "/usr/lib/rstudio-server/bin/rserver" ]
#CMD [ " --server-daemonize=0", "--server-app-armor-enabled=0" ]